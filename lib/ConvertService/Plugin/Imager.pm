package ConvertService::Plugin::Imager;
use Moo;
use namespace::autoclean;

with 'ConvertService::Plugin';

=head1 NAME

ConvertService::Plugin::Imager - ConvertService plugin for images

=head1 DESCRIPTION

This plugin allows ConvertService to convert images into other image formats
or PDF. Resizing is also supported.

By default, images with in-memory sizes up to 250MiB will be allowed to be converted.

This limit can be changed by setting the C<CONVERTER_IMAGER_MAX_SIZE>
environment variable on startup.

=cut

use Function::Parameters qw(:strict);
use Imager;
use File::Temp;
use List::Util qw(any);
use Log::Log4perl qw(:easy);
use PDF::API2;
use Scalar::Util qw(looks_like_number);

Imager->set_file_limits(
    width  => 10_000,
    height => 10_000,
    bytes  => $ENV{CONVERTER_IMAGER_MAX_SIZE} || (250 * 1024 * 1024),
);

=head2 SUPPORTED TYPES

The following types are supported for source files:

=over

=item * JPEG (image/jpeg)

=item * PNG (image/png)

=item * GIF (image/gif)

=item * BMP (image/bmp)

=item * TIFF (image/tiff)

=item * PNM (image/x-portable-pixmap)

=back

=cut

my @SUPPORTED_SOURCE_TYPES = qw(
    image/jpeg
    image/png
    image/gif
    image/bmp
    image/tiff
    image/x-portable-pixmap
);

=pod

And these are supported as the target type:

=over

=item * JPEG (image/jpeg)

=item * PNG (image/png)

=item * GIF (image/gif)

=item * TIFF (image/tiff)

=item * PDF (application/pdf)

=back

=cut

my %SUPPORTED_TARGET_TYPES = (
    'image/jpeg' => 'jpeg',
    'image/png'  => 'png',
    'image/gif'  => 'gif',
    'image/tiff' => 'tiff',
    'application/pdf' => 'pdf',
);

=head1 METHODS

=head2 name

Return the short name of this plugin: C<Imager>.

=cut

method name() { return 'Imager' }

=head2 can_convert(:$from, :$to)

Returns a true value if this plugin can convert files with a MIME type of
C<$from> to C<$to>.

=cut

method can_convert(:$from, :$to) {
    return 1 if     any { $_ eq $from } @SUPPORTED_SOURCE_TYPES
                and any { $_ eq $to } keys %SUPPORTED_TARGET_TYPES;
    return 0;
}

=head2 convert(:$source_file, :$from_type, :$to_type, :$options)

Convert C<$source_file> to C<$to_type>, using the specified C<$options> for the conversion.

C<$from_type> is not used by this plugin.

C<$options> should be a reference to a hash. This plugin understands the
following options:

=over

=item * width

The width, in pixels, of the target image.

=item * height

The height, in pixels, of the target image.

=back

The image will be scaled in a way that keeps the aspect ratio the same.

=cut

method convert(:$source_file, :$from_type, :$to_type, :$options) {
    my $image = Imager->new();
    $image->read(file => $source_file)
        or die "Cannot load $source_file: ", $image->errstr;

    if ($options->{width} || $options->{height}) {
        my $height = looks_like_number($options->{height}) ? int($options->{height}) : undef;
        my $width  = looks_like_number($options->{width})  ? int($options->{width})  : undef;

        DEBUG(sprintf(
            "Resizing image to %s x %s",
            $width  // '<undef>',
            $height // '<undef>'
        ));

        $image = $image->scale(
            $width  ? (xpixels => $width)  : (),
            $height ? (ypixels => $height) : (),
            qtype => 'mixing', # High quality, high speed scaling
            type  => 'min',    # Pick the minimum of (width, height) and scale to that
        );
    }

    # PDF is a special snowflake (it's not really an image format)
    if ($to_type eq 'application/pdf') {
        return $self->_convert_to_pdf($image);
    }

    my $result;
    $image->write(
        data => \$result,
        type => $SUPPORTED_TARGET_TYPES{$to_type},
    );

    return $result;
}

method _convert_to_pdf($source_image) {
    # When the file to convert isn't in a format PDF::API2 understands, we
    # need to convert it first, and use *that* filename to add to the PDF.
    my $info = $self->_file_info_for_pdf($source_image);
    my $loader_method = $info->{loader};

    my $pdf = PDF::API2->new();    
    my $page = $pdf->page;
    $page->mediabox($info->{width}, $info->{height});

    my $pdf_image = $pdf->$loader_method($info->{image_file});

    # Get the page's graphics context, and put the image at the origin (0, 0)
    my $pdf_gfx = $page->gfx;
    $pdf_gfx->image($pdf_image, 0, 0);

    return $pdf->stringify();
}

method _file_info_for_pdf($source_image) {
    # This will be undefined for resized images (which will lead to the "PNG" path below).
    my $source_format = $source_image->tags(name => 'i_format');

    my $intermediate = File::Temp->new();
    
    my $loader;
    if (defined($source_format)
        && any { $source_format eq $_ } qw(jpeg tiff pnm png gif)
    ) {
        $loader = "image_$source_format";
        $source_image->write(file => $intermediate, type => $source_format);
    } else {
        $loader = "image_png";
        $source_image->write(file => $intermediate, type => 'png');
    }
    
    return {
        loader     => $loader,
        image_file => $intermediate,
        width      => $source_image->getwidth(),
        height     => $source_image->getheight(),
    };
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.
