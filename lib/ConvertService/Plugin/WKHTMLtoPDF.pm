package ConvertService::Plugin::WKHTMLtoPDF;
use Moo;
use namespace::autoclean;

with 'ConvertService::Plugin';

=head1 NAME

ConvertService::Plugin::WKHTMLtoPDF - ConvertService plugin to convert HTML to PDF or an image

=head1 DESCRIPTION

This plugin allows ConvertService to convert HTML files into PDF or image
formats. For image formats, specifying a size is possible.

=cut

use autodie qw(:all);
use File::Slurp qw(read_file);
use File::Temp;
use Function::Parameters qw(:strict);
use List::Util qw(any);
use Log::Log4perl qw(:easy);

=head2 SUPPORTED TYPES

This plugin supports HTML ("text/html") source.

=cut

my @SUPPORTED_SOURCE_TYPES = qw(
    text/html
);

=pod

And these types are supported as the target type:

=over

=item * JPEG (image/jpeg)

=item * PNG (image/png)

=item * PDF (application/pdf)

=back

=cut

my %SUPPORTED_TARGET_TYPES = (
    'image/jpeg'      => 'jpg',
    'image/png'       => 'png',
    'application/pdf' => 'pdf',
);

=head2 name

Return the short name of this plugin: C<WKHTMLtoPDF>.

=cut

method name() { return 'WKHTMLtoPDF'; }

=head2 can_convert(:$from, :$to)

Returns a true value if this plugin can convert files with a MIME type of
C<$from> to C<$to>.

=cut

method can_convert(:$from, :$to) {
    return 1 if     any { $_ eq $from } @SUPPORTED_SOURCE_TYPES
                and any { $_ eq $to } keys %SUPPORTED_TARGET_TYPES;
    return 0;
}

=head2 convert(:$source_file, :$from_type, :$to_type, :$options)

Convert C<$source_file> to C<$to_type>, using the specified C<$options> for the conversion.

C<$from_type> is not used by this plugin.

C<$options> should be a reference to a hash. This plugin understands the
following options:

=over

=item * width

The width, in pixels, of the target image.

Default value is 800

=item * height

The height, in pixels, of the target image.

Default value is 600

=back

=cut

method convert(:$source_file, :$from_type, :$to_type, :$options) {
    my $temp = File::Temp->new();

    my @command = ('xvfb-run', '-a', '-s', '-screen 0 1920x1080x24');

    if ($to_type eq 'application/pdf') {
        push @command, (
            'wkhtmltopdf',
            '-n',       # Disable JS
            '-q', # Be quiet
            '-s', 'A4', # A4 paper
            $source_file, $temp->filename,
        );
    } else {
        push @command, (
            'wkhtmltoimage',
            '-n', # Disable JS
            '-q', # Be quiet
            '-f'       => $SUPPORTED_TARGET_TYPES{$to_type},
            '--width'  => $options->{width} // 800,
            '--height' => $options->{height} // 600,
            $source_file, $temp->filename,
        );
    }

    DEBUG("Running wkhtml command: '" . join(" ", @command) . "'");
    system(@command);

    my $result = read_file($temp->filename, binmode => ':raw');
    return $result;
}

1;

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.
