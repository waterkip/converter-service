FROM perl:5.26

WORKDIR /usr/src/converter
COPY cpanfile /usr/src/converter/cpanfile

RUN useradd -ms /bin/bash converter -d /home/converter

RUN    apt update \
    && apt -y --no-install-recommends install \
        libpng-dev libtiff-dev libgif-dev libjpeg-dev libmagic-dev \
        libgirepository1.0-dev libcairo2-dev libpoppler-glib-dev \
        fonts-crosextra-caladea fonts-crosextra-carlito fonts-croscore \
        fonts-freefont-ttf fonts-liberation2 fonts-ocr-a fonts-ocr-b \
        fonts-dejavu fonts-texgyre fonts-lato \
        xfonts-scalable \
        unoconv libreoffice-writer libreoffice-calc \
        xvfb xauth libgl1-mesa-dri \
        wkhtmltopdf poppler-utils \
    && cpanm --mirror http://mirror.nl.leaseweb.net/CPAN/ --installdeps . \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf ~/.cpanm

COPY . /usr/src/converter

EXPOSE 5032

USER converter
CMD [ "bin/run_starman.pl", "--port", "5032", "bin/converter-service.pl" ]
