#! /usr/bin/env perl
use warnings;
use strict;

use Test::More;

use autodie;
use ConvertService::Plugin::OfficeFormats;
use Image::ExifTool qw(ImageInfo);
use FindBin;
use File::Copy;

my $csi = ConvertService::Plugin::OfficeFormats->new();

my @word_processor_types = qw(
    text/plain
    text/html
    text/rtf
    application/msword
    application/vnd.oasis.opendocument.text
    application/vnd.openxmlformats-officedocument.wordprocessingml.document
);

my @spreadsheet_types = qw(
    text/csv
    text/plain
    application/vnd.ms-excel
    application/vnd.oasis.opendocument.spreadsheet
    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
);

ok(
    !$csi->can_convert(from => 'application/msword', to => 'application/vnd.ms-excel'),
    "Cannot convert between 'text' and 'spreadsheet' types"
);

for my $source (@word_processor_types) {
    for my $dest (@word_processor_types, 'application/pdf') {
        if ($source eq $dest || ($source eq 'text/html' && $dest eq 'application/pdf')) {
            ok( 
                !$csi->can_convert(from => $source, to => $dest),
                "Cannot convert from $source to $dest"
            );
        }
        else {
            ok( 
                 $csi->can_convert(from => $source, to => $dest),
                "Can convert from $source to $dest"
            );
        }
        ok(
            !$csi->can_convert(from => "application/pdf", to => $dest),
            "Cannot convert from application/pdf to image/$dest"
        );
        ok(
            !$csi->can_convert(from => $source, to => "video/mp4"),
            "Cannot convert from $source to video/mp4"
        );
    }
}

for my $source (@spreadsheet_types) {
    for my $dest (@spreadsheet_types, 'application/pdf') {
        if ($source eq $dest) {
            ok( 
                !$csi->can_convert(from => $source, to => $dest),
                "Cannot convert from $source to $dest"
            );
        }
        else {
            ok( 
                 $csi->can_convert(from => $source, to => $dest),
                "Can convert from $source to $dest"
            );
        }
        ok(
            !$csi->can_convert(from => "application/pdf", to => $dest),
            "Cannot convert from application/pdf to image/$dest"
        );
        ok(
            !$csi->can_convert(from => $source, to => "video/mp4"),
            "Cannot convert from $source to video/mp4"
        );
    }
}

# Document conversion
{
    my $source = File::Temp->new( SUFFIX => '.docx' );
    copy("$FindBin::Bin/../t/data/Word.docx", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        to_type     => 'application/msword',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/msword', 'DOCX to DOC conversion succeeded');
}

{
    my $source = File::Temp->new( SUFFIX => '.doc' );
    copy("$FindBin::Bin/../t/data/LibreOffice.doc", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'application/msword',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'DOC to PDF conversion succeeded');
}

{
    my $source = File::Temp->new( SUFFIX => '.odt' );
    copy("$FindBin::Bin/../t/data/LibreOffice.odt", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'application/vnd.oasis.opendocument.text',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'ODT to PDF conversion succeeded');
}

{
    my $source = File::Temp->new( SUFFIX => '.odt' );
    copy("$FindBin::Bin/../t/data/Manual-with-embedded-font.odt", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'application/vnd.oasis.opendocument.text',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'ODT to PDF conversion succeeded');

    # I *think* this can be done using the Poppler library, but I can't figure
    # out how to work the font_info/iter stuff from Perl
    my $dest = File::Temp->new( SUFFIX => '.pdf' );
    binmode($dest, ':raw');
    print $dest $converted;
    close $dest;

    open my $pdffont, "-|", "/usr/bin/pdffonts", $dest->filename;
    my $pdffont_out = do {
        local $/;
        <$pdffont>
    };

    like(
        $pdffont_out,
        qr{TestStuffnormal\s+TrueType\s+WinAnsi\s+yes\s+yes\s+yes},
        "PDF has the right embedded font"
    );
}

{
    my $source = File::Temp->new( SUFFIX => '.xls' );
    copy("$FindBin::Bin/../t/data/LibreOffice.xls", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'application/vnd.ms-excel',
        to_type     => 'application/pdf',
        options     => { },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');

    is($info->{MIMEType}, 'application/pdf', 'XLS to PDF conversion succeeded');
}

{
    my $source = File::Temp->new( SUFFIX => '.csv' );
    copy("$FindBin::Bin/../t/data/example.csv", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'text/csv',
        to_type     => 'application/vnd.ms-excel',
        options     => {
            locale => "nl_NL",
            column_types => [
                "datetime",
                "numeric",
                "string",
            ],
        },
    );

    my $info = ImageInfo(\$converted, 'MIMEType');
    is($info->{MIMEType}, 'application/vnd.ms-excel', 'CSV to XLS conversion succeeded');
}


{
    my $source = File::Temp->new( SUFFIX => '.xlsx' );
    copy("$FindBin::Bin/../t/data/Excel.xlsx", $source->filename);

    my $converted = $csi->convert(
        source_file => $source->filename,
        from_type   => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        to_type     => 'text/csv',
        options     => {
            locale => "nl_NL",
        },
    );

    is($converted, <<EOT, "XLS to CSV conversion succeeded");
Excel Document 1,
,
10,30
20,70
30,110
40,150
50,190
60,230
70,270
80,310
90,350
100,390
110,430
120,470
130,510
140,550
150,590
EOT
}

done_testing();
