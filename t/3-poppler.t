#! /usr/bin/env perl
use warnings;
use strict;

use Test::More;
use ConvertService::Plugin::Poppler;
use Image::ExifTool qw(ImageInfo);
use FindBin;

my $csi = ConvertService::Plugin::Poppler->new();

ok(
    $csi->can_convert(from => "application/pdf", to => "image/png"),
    "Can convert from application/pdf to image/png"
);

ok(
    !$csi->can_convert(from => "application/pdf", to => "image/gif"),
    "Cannot convert from application/pdf to image/gif"
);

ok(
    !$csi->can_convert(from => "application/octet-stream", to => "image/png"),
    "Cannot convert from application/octet-stream to image/png"
);

# Convert an actual PDF to PNG
{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.pdf",
        from_type   => 'application/pdf',
        to_type     => 'image/png',
        options     => {},
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');

    is($info->{MIMEType}, 'image/png', 'Conversion to PNG succeeded');

    is($info->{ImageWidth},  595, 'Image width is 595');
    is($info->{ImageHeight}, 842, 'Image height is 842');
}

# Convert an actual PDF to PNG - with size
{
    my $converted = $csi->convert(
        source_file => "$FindBin::Bin/../t/data/example.pdf",
        from_type   => 'application/pdf',
        to_type     => 'image/png',
        options     => { width => 320, height => 240 },
    );

    my $info = ImageInfo(\$converted, 'MIMEType', 'ImageWidth', 'ImageHeight');

    is($info->{MIMEType}, 'image/png', 'Conversion to PNG succeeded');

    is($info->{ImageWidth},  169, 'Image width scaled to 169 (best fit)');
    is($info->{ImageHeight}, 240, 'Image height scaled to 240 (best fit)');
}

done_testing();