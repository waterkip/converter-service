#!/usr/bin/env perl

use strict;
use warnings;
use FindBin;
use lib "$FindBin::Bin/../lib";

use ConvertService::Web;
use File::Copy;
use File::Path qw(make_path);
use Plack::Builder;

# Drop a default LibreOffice configuration:
my $lo_config_dir = "$ENV{HOME}/.config/libreoffice/4/user";
if (!-d $lo_config_dir) {
    make_path($lo_config_dir);

    copy("etc/registrymodifications.xcu", "$lo_config_dir/registrymodifications.xcu")
        or die "Could not set LibreOffice defaults: $!";
}

builder {
    ConvertService::Web->to_app;
}

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the C<CONTRIBUTORS> file.

ConvertService uses the EUPL license, for more information please have a look
at the C<LICENSE> file.
